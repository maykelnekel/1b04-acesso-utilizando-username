import { useState } from "react"
import './style.css'
function GetUserComponent({ setUser, setIsLoggedIn }) {
    const [userInput, setUserInput] = useState('')
    const HandleLogin = () => {
        setUser(userInput)
        setIsLoggedIn(true)
    }
    return (
        <form>
            <h2>Faça seu Login</h2>
            <input
                type="text"
                Placeholder='Insira seu nome'
                value={userInput}
                onChange={(event) => setUserInput(event.target.value)}
            />
            <br/>
            <button onClick={() => HandleLogin(userInput)}>Acessar com o nome</button>
        </form>
    )
}
export default GetUserComponent