function WelcomePage ({ user, setIsLoggedIn }) {
    const HandleLogout = () => setIsLoggedIn(false)
    
    return (
        <div>
            <h1>Bem vindo {user}!</h1>
            <button onClick={() => HandleLogout()} >Logout</button>
        </div>
    )
}
export default WelcomePage